var express = require('express')
var app = express() // Aqui eu importo o express
var consign = require('consign')

app.set('view engine', 'ejs')
app.set('views', './views')

app.use(express.static('./public'))

consign()
    .include('app/routes')
    .into(app)

app.use(express.static('./public'))

consign()
    .include('app/routes')
    .into(app)

module.exports = app