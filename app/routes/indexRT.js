var data = require('./data.json')

module.exports = function(app){

app.get('/',function(req, res){
    res.render('index', { data } )
})

app.get('/clientes',function(req, res){
    res.render('clientes', { data})
})

app.get('/pedidos',function(req, res){
    res.render('pedidos', {data})
})
}